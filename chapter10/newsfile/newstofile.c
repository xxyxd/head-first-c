#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

void error(char *msg)
{
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

int main(int argc, char *argv[]) {
    int pid_status; 
    char *phrase = argv[1];
    FILE *f = fopen("stories.txt", "w");
    if (!f) 
    {
        error("Can't open stories.txt");
    }
    pid_t pid = fork(); 
    if (pid == -1)
    {
        error("Can't fork process");
    }
    if(!pid)
    {
        if (dup2(fileno(f), 1) == -1)
        {
            error("Can't redirect Standard Output");
        }
        if (execlp("bash", "bash", "./echo.sh", phrase, NULL) == -1)
        {
            error("Can't run script"); 
        }
    }
    if (waitpid(pid, &pid_status, 0) == -1)
    {
        error("Error while waiting");
    }
    if (WEXITSTATUS(pid_status)) 
        puts("Error status non-zero");
    return 0;
}
