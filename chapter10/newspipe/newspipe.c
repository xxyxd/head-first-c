#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

void open_url(char *url) 
{
    char launch[255];
    sprintf(launch, "open '%s'", url);
    printf("%s", launch);
    // system(launch);
}

void error(char *msg)
{
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

int main(int argc, char *argv[]) {
    int pid_status, fd[2];
    char *phrase = argv[1];
    char line[255];
    if ( pipe(fd) == -1)
    {
        error("Can't create the pipe");
    }
    pid_t pid = fork(); 
    if (pid == -1)
    {
        error("Can't fork process");
    }
    if(!pid)
    {
        dup2(fd[1], 1);
        close(fd[0]);
        if (execlp("wget", "wget", phrase, NULL) == -1)
        {
            error("Can't run script"); 
        }
    }
    dup2(fd[0], 0);
    close(fd[1]);
    while (fgets(line ,255 ,stdin)) 
    {
        // printf("%s", line);
        open_url(line);
    }
    return 0;
}
