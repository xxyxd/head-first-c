#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>


struct addrinfo *res;
struct addrinfo hints;
memset(&hints, 0, sizeof(hints));
hints.ai_family = PF_UNSPEC;
hints.ai_socktype = SOCK_STREAM;
getaddrinfo("www.oreilly.com", "80", &hints, &res);
