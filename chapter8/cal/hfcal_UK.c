#include<stdio.h>
#include"hfcal.h"

void display_cal(float weight, float distance, float corff)
{
    printf("Weight: %3.2f kg\n", weight / 2.2064);
    printf("Distance: %3.2f km\n", distance * 1.609344);
    printf("Call burned: %4.2f cal \n", corff * weight * distance);
}
