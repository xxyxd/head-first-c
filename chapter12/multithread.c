#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

void error(char *msg)
{
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

void* first_one(void *a)
{
	int i = 0;
	for (i = 0; i < 5; i++)
	{
		puts("First one");
		sleep(1);
	}
	return NULL;
}

void* second_one(void *a)
{
	int i = 0;
	for (i = 0; i < 5; i++)
	{
		puts("Second one");
		sleep(1);
	}
	return NULL;
}

int main()
{

	pthread_t t0;
	pthread_t t1;
	if (pthread_create(&t0, NULL, first_one, NULL) == -1)
		error("无法创建线程t0");
	if (pthread_create(&t1, NULL, second_one, NULL) == -1)
		error("无法创建线程t1");

	void* result;
	if (pthread_join(t0, &result) == -1)
		error("无法回收线程t0");
	if (pthread_join(t1, &result) == -1)
		error("无法收回线程t1");
	return 0;

}